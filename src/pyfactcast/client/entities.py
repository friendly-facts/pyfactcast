from typing import Type, TypeVar, Dict, List, Optional
from uuid import UUID, uuid4
from pydantic.fields import Field

from pyfactcast.grpc.generated.FactStore_pb2 import MSG_Notification
from pydantic import BaseModel, Extra, constr

import json

F = TypeVar("F", bound="Fact")


class CatchUp:
    pass


# Sadly pydantic and mypy do not play nice here.
AggsId: constr = constr(  # type: ignore
    regex=r"^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$"
)


class FactHeader(BaseModel):
    class Config:
        extra = Extra.allow
        validate_assignment = True

    ns: str
    type: str
    id: UUID = Field(default_factory=uuid4)
    aggIds: Optional[List[AggsId]] = None  # type: ignore
    meta: Dict[str, str] = {}


class Fact(BaseModel):
    header: FactHeader
    payload: Dict

    @classmethod
    def from_msg(cls: Type[F], msg: MSG_Notification) -> F:
        return cls(
            header=json.loads(msg.fact.header), payload=json.loads(msg.fact.payload)
        )


class VersionedType(BaseModel):
    name: str
    version: int = 0


class SubscriptionSpec(BaseModel):
    ns: str
    type: Optional[VersionedType] = None

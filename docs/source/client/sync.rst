Synchronous Client
==================

.. contents::
  :local:
  :backlinks: none

.. autoclass:: pyfactcast.client.sync.FactStore
  :members:

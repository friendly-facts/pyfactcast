import nox
from nox import Session


@nox.session(python=["3.8", "3.7"])
def tests(session: Session) -> None:
    args = session.posargs or ["--cov=pyfactcast"]
    session.run("poetry", "install", external=True)
    session.run("pytest", *args)


@nox.session
def docs(session: Session) -> None:
    session.run("poetry", "install", external=True)
    session.chdir("docs")
    session.run("rm", "-rf", "build/html", external=True)
    sphinx_args = ["-b", "html", "./source", "./build/html"]

    if "serve" in session.posargs:
        session.run(
            "sphinx-autobuild",
            "--port",
            "5000",
            "--open-browser",
            *sphinx_args,
            external=True
        )
    else:
        session.run("sphinx-build", *sphinx_args, external=True)

from typer.testing import CliRunner

from pyfactcast.app.ui.cli import app

runner = CliRunner()


def test_namespaces(mocker):
    def namespaces_fake(fact_store=None):
        return ["the-fakes"]

    mocker.patch("pyfactcast.app.ui.enumerate.business.namespaces", namespaces_fake)

    result = runner.invoke(app, ["enumerate", "namespaces"])
    assert result.exit_code == 0
    assert "the-fakes" in result.stdout


def test_types(mocker):
    def types_fake(a, fact_store=None):
        return ["the-fakes"]

    mocker.patch("pyfactcast.app.ui.enumerate.business.types", types_fake)

    result = runner.invoke(app, ["enumerate", "types", "nope"])
    assert result.exit_code == 0
    assert "the-fakes" in result.stdout

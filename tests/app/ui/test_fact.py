from pathlib import Path
from typer.testing import CliRunner

from pyfactcast.app.ui.cli import app

runner = CliRunner()


def test_serial_of_with_result(mocker):
    def serial_fake(uuid, fact_store):
        return "12334"

    mocker.patch("pyfactcast.app.ui.fact.business.serial_of", serial_fake)

    result = runner.invoke(
        app, ["fact", "serial-of", "85517d5a-7522-11eb-9304-924392e1c30a"]
    )
    assert result.exit_code == 0
    assert "12334" in result.stdout


def test_serial_of_without_result(mocker):
    def serial_fake(uuid, fact_store):
        return None

    mocker.patch("pyfactcast.app.ui.fact.business.serial_of", serial_fake)

    result = runner.invoke(
        app, ["fact", "serial-of", "85517d5a-7522-11eb-9304-924392e1c30a"]
    )
    assert result.exit_code == 0
    assert "85517d5a-7522-11eb-9304-924392e1c30a" in result.stdout
    assert "No fact" in result.stdout
    assert "found" in result.stdout


def test_publish_with_non_existent_file(mocker):
    def publish_fake(path, fact_store):
        return None

    mocker.patch("pyfactcast.app.ui.fact.business.publish", publish_fake)

    result = runner.invoke(app, ["fact", "publish", "i_dont_exist"])
    assert result.exit_code == 2
    assert "does not exist" in result.stdout


def test_publish(mocker):
    def publish_fake(path, fact_store):
        return None

    mocker.patch("pyfactcast.app.ui.fact.business.publish", publish_fake)
    fact_file_path = (
        Path(__file__)
        .parent.parent.parent.absolute()
        .joinpath("resources")
        .joinpath("facts.json")
    )
    result = runner.invoke(app, ["fact", "publish", str(fact_file_path)])
    assert result.exit_code == 0

from typer.testing import CliRunner

from pyfactcast.app.ui.cli import app

runner = CliRunner()


def test_has_a_verbose_option():
    result = runner.invoke(
        app,
    )
    assert result.exit_code == 0
    assert "verbose" in result.stdout


def test_can_enumerate_namespaces():
    result = runner.invoke(app, ["enumerate"])
    assert result.exit_code == 0
    assert "namespaces" in result.stdout


def test_can_subscribe_to_streams():
    result = runner.invoke(app, ["streams"])
    assert result.exit_code == 0
    assert "subscribe" in result.stdout

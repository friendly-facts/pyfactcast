from uuid import uuid4
from pyfactcast.app.business.entities import CollectSpec
from pyfactcast.client.entities import Fact, FactHeader
from typing import Iterable
from pyfactcast.app.business.streams import (
    _filter_types,
    collect,
    collect_by_namespace,
    subscribe,
)
import pytest


class FakeFactStore:
    def __enter__(self):
        return self

    def __exit__(
        self,
        exc_type,
        exc_value,
        traceback,
    ):
        pass

    def enumerate_types(self, namespace: str) -> Iterable[str]:
        return ["a", "b", "ab", "abba", "bbaa"]

    def subscribe(
        self, subscription_specs, continuous=False, from_now=False, after_fact=None
    ):
        return [
            Fact(
                header=FactHeader(ns="testing", type="a", id=str(uuid4())),
                payload={"fake": "data"},
            )
        ]


def test_filters_with_full_matching():
    res = _filter_types(fs=FakeFactStore(), namespace="fake", it=["a"])
    assert len(res) == 1
    assert list(res) == ["a"]


def test_returns_empty_on_empty():
    res = _filter_types(fs=FakeFactStore(), namespace="fake", it=[])
    assert len(res) == 0


def test_simple_type_based_subscription():
    res = subscribe(
        namespace="testing", follow=False, fact_types=["a"], fact_store=FakeFactStore()
    )
    assert len(res) == 1


def test_type_and_version_based_subscription():
    res = subscribe(
        namespace="testing",
        follow=False,
        fact_types=["a"],
        type_versions=[1],
        fact_store=FakeFactStore(),
    )
    assert len(res) == 1


def test_errors_out_if_to_many_versions():
    with pytest.raises(ValueError):
        subscribe(
            namespace="testing",
            follow=False,
            fact_types=["hello"],
            type_versions=[1, 2, 3],
            fact_store=FakeFactStore(),
        )


def test_collect():
    res = collect([CollectSpec(ns="testing", type="a")], fact_store=FakeFactStore())
    assert len(res) == 1


def test_collect_by_namespace():
    res = collect_by_namespace(
        [CollectSpec(ns="testing", type="a")], fact_store=FakeFactStore()
    )
    assert len(res["testing"]) == 1


# TODO more and better tests

import logging
import pytest
from pyfactcast.client.config import (
    ClientConfiguration,
    Credentials,
    get_client_configuration,
    _get_client_config_from_file,
)


from os import environ as env
from pathlib import Path


def test_credentials_can_be_instantiated():
    creds = Credentials("user", "pass")

    assert creds.password == "pass"
    assert creds.username == "user"


def test_credentials_requires_username():
    with pytest.raises(ValueError):
        Credentials("", "pass")


def test_credentials_requires_password():
    with pytest.raises(ValueError):
        Credentials("user", "")


def test_credentials_requires_user_and_password():
    with pytest.raises(ValueError):
        Credentials("", "")


def test_client_config_requires_server_string():
    with pytest.raises(ValueError):
        ClientConfiguration("")


def test_client_config_can_be_instantiated():
    config = ClientConfiguration(
        server="localhost:5001",
        ssl_target_override="local.host",
        credentials=Credentials("user", "pass"),
        root_cert_path="/tmp/nope",
    )

    assert config.server == "localhost:5001"


def test_getting_client_config_just_server():
    conf = get_client_configuration()

    assert conf.server == "localhost:5001"


def test_getting_client_config():
    env["GRPC_USER"] = "user"
    env["GRPC_PASSWORD"] = "pass"
    conf = get_client_configuration()

    assert conf.server == "localhost:5001"
    assert conf.credentials.username == "user"
    assert conf.credentials.password == "pass"

    del env["GRPC_USER"]
    del env["GRPC_PASSWORD"]


def test_reading_config_file():
    location = (
        Path(__file__)
        .parent.parent.absolute()
        .joinpath("resources")
        .joinpath("sample_config.json")
    )

    res = _get_client_config_from_file(location)

    assert res["profiles"]["default"]["grpc_server"] == "my.eventstore.home.local"


def test_non_existing_config_file_returns_none(caplog):
    with caplog.at_level(logging.DEBUG):
        res = _get_client_config_from_file(Path("/i-dont-exist"))

    assert res is None
    assert "No configuration file found" in caplog.text


def test_raises_value_error_for_non_existing_profile(mocker):
    def fake_get_client_config_from_file():
        return {"profiles": {"test": {}}}

    mocker.patch(
        "pyfactcast.client.config._get_client_config_from_file",
        fake_get_client_config_from_file,
    )

    with pytest.raises(ValueError) as e:
        get_client_configuration(profile="anyone")

    assert "The specified profile" in str(e)


def test_replacement_of_file_vars_through_env(mocker):
    def fake_get_client_config_from_file():
        return {
            "profiles": {
                "default": {
                    "grpc_server": "my.eventstore.home.local",
                    "grpc_root_cert_path": "path",
                    "grpc_cn_overwrite": "my.eventstore.local",
                    "grpc_user": "user",
                    "grpc_pass": "pass",
                    "grpc_insecure": False,
                }
            }
        }

    mocker.patch(
        "pyfactcast.client.config._get_client_config_from_file",
        fake_get_client_config_from_file,
    )

    env["GRPC_USER"] = "username"
    env["GRPC_PASSWORD"] = "password"

    conf = get_client_configuration()

    assert conf.server == "localhost:5001"
    assert conf.credentials.username == "username"
    assert conf.credentials.password == "password"

    assert conf.ssl_target_override == "my.eventstore.local"
    assert conf.root_cert_path == "path"
    assert not conf.insecure

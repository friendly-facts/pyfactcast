from pyfactcast.client.auth.basic import BasicAuth


def test_auth_string_generation():
    ba = BasicAuth("user", "pass")
    assert ba.auth_string == "Basic dXNlcjpwYXNz"


def test_auth_callback():
    def callback(result, _):
        assert result[0] == ("authorization", "Basic dXNlcjpwYXNz")

    ba = BasicAuth("user", "pass")
    ba(None, callback)

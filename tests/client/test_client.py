import pytest
from uuid import uuid4
from pyfactcast.client.entities import Fact, FactHeader, SubscriptionSpec, VersionedType
from pyfactcast.client.sync import FactStore

FACT_UUID = uuid4()


@pytest.fixture(scope="module")
def prefilled_factcast(local_factcast: FactStore):
    facts = [
        Fact(
            header=FactHeader(ns="testNamespace", id=str(FACT_UUID), type="testType"),
            payload={"junk": "event"},
        ),
        Fact(
            header=FactHeader(ns="testNamespaceA", type="testTypeA"),
            payload={"junk": "event"},
        ),
        Fact(
            header=FactHeader(ns="testNamespaceA", type="testTypeA"),
            payload={"junk": "event"},
        ),
        Fact(
            header=FactHeader(ns="testNamespaceA", type="testTypeB"),
            payload={"junk": "event"},
        ),
        Fact(
            header=FactHeader(ns="testNamespaceA", type="testTypeC"),
            payload={"junk": "event"},
        ),
    ]

    with local_factcast as fc:
        for fact in facts:
            fc.publish(fact=fact)

    return local_factcast


@pytest.mark.containers
def test_simple_subscription(prefilled_factcast: FactStore):
    with prefilled_factcast as fc:
        result = fc.subscribe(subscription_specs=[SubscriptionSpec(ns="testNamespace")])

        assert len(list(result)) == 1


@pytest.mark.containers
def test_type_subscription(prefilled_factcast: FactStore):
    with prefilled_factcast as fc:
        result = fc.subscribe(
            subscription_specs=[
                SubscriptionSpec(
                    ns="testNamespaceA", type=VersionedType(name="testTypeA")
                )
            ]
        )

        assert len(list(result)) == 2


@pytest.mark.containers
def test_mixed_type_same_ns_subscription(prefilled_factcast: FactStore):
    with prefilled_factcast as fc:
        result = fc.subscribe(
            subscription_specs=[
                SubscriptionSpec(
                    ns="testNamespaceA", type=VersionedType(name="testTypeA")
                ),
                SubscriptionSpec(
                    ns="testNamespaceA", type=VersionedType(name="testTypeB")
                ),
            ]
        )

        assert len(list(result)) == 3


@pytest.mark.containers
def test_mixed_type_cross_ns_subscription(prefilled_factcast: FactStore):
    with prefilled_factcast as fc:
        result = fc.subscribe(
            subscription_specs=[
                SubscriptionSpec(
                    ns="testNamespaceA", type=VersionedType(name="testTypeA")
                ),
                SubscriptionSpec(ns="testNamespace"),
            ]
        )

        assert len(list(result)) == 3


@pytest.mark.containers
def test_namespace_enumeration(prefilled_factcast: FactStore):
    with prefilled_factcast as fc:
        namespaces = fc.enumerate_namespaces()
    assert len(namespaces) == 2
    assert "testNamespace" in namespaces


@pytest.mark.containers
def test_type_enumeration(prefilled_factcast: FactStore):
    with prefilled_factcast as fc:
        types = fc.enumerate_types("testNamespace")
    assert len(types) == 1
    assert "testType" in types


@pytest.mark.containers
def test_serial_of_existing_is_returned(prefilled_factcast: FactStore):
    with prefilled_factcast as fc:
        serial = fc.serial_of(fact_id=FACT_UUID)
    assert serial


@pytest.mark.containers
def test_serial_of_non_existing_returns_none(prefilled_factcast: FactStore):
    with prefilled_factcast as fc:
        serial = fc.serial_of(fact_id=uuid4())
    assert serial is None

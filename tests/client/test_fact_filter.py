import logging

from pyfactcast.client.sync import _fact_filter


def test_returns_true_if_notification_type_is_fact(fact_msg_notification):
    assert _fact_filter(fact_msg_notification)


def test_logs_on_catchup(catchup_msg_notification, caplog):
    with caplog.at_level(logging.DEBUG):
        assert _fact_filter(catchup_msg_notification) is False
        assert "Caught up" in caplog.text


def test_logs_on_complete(complete_msg_notification, caplog):
    with caplog.at_level(logging.DEBUG):
        assert _fact_filter(complete_msg_notification) is False
        assert "stream complete" in caplog.text


def test_logs_on_multi_fact(facts_msg_notification, caplog):
    with caplog.at_level(logging.DEBUG):
        assert _fact_filter(facts_msg_notification) is False
        assert "not implemented" in caplog.text


def test_logs_on_unknown_msg_type(non_existant_msg_notification, caplog):
    with caplog.at_level(logging.DEBUG):
        assert _fact_filter(non_existant_msg_notification) is False
        assert "Received Notification of type" in caplog.text

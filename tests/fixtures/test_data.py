import json
from pyfactcast.client.config import ClientConfiguration
from pyfactcast.grpc.generated.FactStore_pb2 import (
    MSG_Fact,
    MSG_Facts,
    MSG_Notification,
)
import pytest

from os import environ as env


@pytest.fixture(scope="session", autouse=True)
def set_server_string():
    env["GRPC_SERVER"] = "localhost:5001"


@pytest.fixture(scope="session")
def fact_msg_notification():
    return MSG_Notification(
        type=MSG_Notification.Fact,
        fact=MSG_Fact(
            header=json.dumps({"mock": "header"}),
            payload=json.dumps({"mock": "payload"}),
        ),
    )


@pytest.fixture(scope="session")
def catchup_msg_notification():
    return MSG_Notification(
        type=MSG_Notification.Catchup,
    )


@pytest.fixture(scope="session")
def complete_msg_notification():
    return MSG_Notification(
        type=MSG_Notification.Complete,
    )


@pytest.fixture(scope="session")
def facts_msg_notification():
    return MSG_Notification(
        type=MSG_Notification.Facts,
        facts=MSG_Facts(
            fact=[
                MSG_Fact(
                    header=json.dumps({"mock": "header"}),
                    payload=json.dumps({"mock": "payload"}),
                )
            ]
        ),
    )


@pytest.fixture(scope="session")
def non_existant_msg_notification():
    return MSG_Notification(
        type=10,
    )


@pytest.fixture(scope="session")
def local_fc_config():
    return ClientConfiguration(server="localhost:5001")

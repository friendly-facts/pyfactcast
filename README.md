# PyFactCast

Welcome to pyfactcast. You can find more extensive documentation over at [readthedocs](https://pyfactcast.readthedocs.io/en/latest/).

This project arose manly out of frustration with the excessive wait times for a spring boot
based CLI like the one offered by the original [FactCast](https://docs.factcast.org/).
But on the way grew a bit. It will likely grow even more as I would like to actually use this
tooling to bring the ability for rapid prototyping and production grade python applications
to the FactCast community.

Contributions are welcome. Just get in touch.

## Quickstart

Simply `pip install pyfactcast` and get going. The cli is available as `factcast` and
you can run `factcast --help` to get up to speed on what you can do.

## Development

This project uses `poetry` for dependency management and `pre-commit` for local checks.
